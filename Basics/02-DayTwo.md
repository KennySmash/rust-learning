# Some Rust Basics

so taking a look at some simple stuff today starting with ...

### Variables

so variables in rust are pretty simple:

```rust
let imAVariable = 500;
```

you can do all the usual things like assigning them however one thing to note is that they are immutable by default so that value is locked in for life, if we want to change that and make it mutable or assignable during runtime all we have to do is add the `mut` qualification to it (dont know what they are called yet, attributes maybe?)

```rust
let mut iCanChange = 9001
```

lets take a look at what we can pack in them though

### Data types
#### Integers
we got integers in the signed and unsigned variety and flavours of 8, 16, 32, 64 bits. we all know what integers are, 8-bit can run from 0 to 255, 16-bit can run from -32768 to 32767 etc.  

aparently we need to keep this and what cpu arch we are targeting in mind

#### Floats
we can use floats in 32 or 64 bit variety

#### Booleans
`bool` quite simply `true` or `false`

#### Characters
the `char` type lets us have a single unicode character

### Logic

perhaps the most important tools in a programmers toolbox is control flow so no surprise we have some top class ones in rust:

#### Conditions
we got `if` and `else` here 
```rust
let x = 5;
let y = 10;

if x > y {
    println!("{} > {}", x, y);
} else {
    println!("{} <= {}", x, y);
}
```

#### Loops
loops are pretty important so we gotta know about them too:

```rust
let mut a = 100;
let mut b = 200;

while b != 0 {
    let temp = b;
    b = a & b;
    a = temp;
}
println!("The greatest common divisor of 100 and 200 is: {}", a);
```
---

Tomorrow I'm gonna take a look at functions