# Basics of Rust  

Rust is pretty well supported on most platforms and you can download it [here](https://rustup.rs)  
it installs the basic rust tools and side note, rust runs on a pretty quick dev cycle so new features and fixes come out every six weeks  

---

## New projects
we can start projects using the `cargo` tool, just simply run 
```
$ cargo new kickass-tutorial
```
 and it will set up a folder at the `$PWD` called kickass-tutoral and then it will bootstrap the skeleton for us and in there will be the `Cargo.toml` and a `src` directory (where your source code goes) along with a git init too  

 the `Cargo.toml` file is what houses our dependancies and crates similar to how `packages.json` is used in the JS world  

 and heres the hallowed (heh) hello world in rust:
 ```rs
 fn main() {
    println!("Hello World!");
 }
 ```

 `fn` denotes the start of a named function and most programs will have a `main()` function as the entrypoint of the program, `println()` outputs a line to `stdout` which in scary terminal speak is just a buffer that will send the text to your screen or if you're a time traveler - your Teletype Model 33 will print it to the paper spool.  

 For real, your fancy computer terminal is just cosplaying as an old school printer with a keeb attached

 besides that point - lets get our hello world to do something. From the root of your kickass-tutorial folder, run this:
 ```sh
$ cargo build
 ```

 and just like that you'll get a nice executable to give to friends but if you want to run it for yourself you can just go:
 ```sh
 $ cargo run
 ```
 and you'll get a nice cheerful `Hello World!` in your terminal

 ---

 Next up tomorrow I'm going to look at the types of rust projects and how they should be managed