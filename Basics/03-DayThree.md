# Functions, Am I right?


Rusty functions make use of the `fn` keyword and the rules for function names are:
- standard english letters ( A-Z a-z )
- digits ( 0 - 9 )
- underscores ( _ )
- cant start with a digit
- cant start with a lone underscore as that means something else

then we can pass in a list of parameters inside some parenthesis, if it dont need em, skip em

and then a set of braces for the execution block

it goes without saying that we are going to be writing a lot of functions that will not be the `main()` function so for these we can take a look at ...

## Modules

So modules are the way that we can organise our functions ( and data structs but thats for later ) 

defining a module is pretty easy, in one of our .rs file:

```rs
pub mod helperModule {
    pub fn taskX(){
        someCodeHere...
    }

    pub fn taskY(){
        strangeItsMoreCode...
    }
}
```

the `pub` bit is to indicate that its interface is public and not to worry yet cause interfaces will be coming later.

we would use the above pattern if we were going to run multiple modules in the same file. we should be keeping them to their own files though so we would do something like this: 

```rs
// ./helperModuleDeux/mod.rs or ./helperModuleDeux.rs
pub fn taskX(){
    ...
}

pub fn taskY(){
    ...
}
```

and then we can then load it up via:
```rs
pub mod helperModuleDeux;
```
or for a particular function 
```rs
helperModuleDeux::taskX()
```

## Some Random thoughts

so in rust, functional blocks need a `;` after all expressions apart from the last one, but the guidelines say that if you add it to the last line then the function inherrently returns an empty tuple (kind of like an object from JS)